<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student11
  Date: 7/14/20
  Time: 8:36 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Wishes</title>
</head>
<body>

<h3>Here are all the Wishes: </h3>
<table border="1">
    <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Price</th>
    </tr>

    <c:forEach items="${ListOfWishes}" var="enrolledWishLists">

        <tr>
            <td>${enrolledWishLists.name}</td>
            <td>${enrolledWishLists.category}</td>
            <td>${enrolledWishLists.price}</td>
        </tr>

    </c:forEach>

</table>

</body>
</html>
