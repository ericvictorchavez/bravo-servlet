<%--
  Created by IntelliJ IDEA.
  User: student11
  Date: 7/14/20
  Time: 10:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add WishList</title>
</head>
<body>

<h3>Enter the new WishList info:</h3>
<form action="/WishLists/add-WishLists" method="post">
    <label for="name"> Name</label>
    <input type="text" name="Name" id="Name">
    <br>
    <label for="category">Category</label>
    <input type="text" name="Category" id="Category">
    <br>
    <label for="price">Price</label>
    <input type="text" name="Price" id="Price">
    <br>
    <input type="submit">

</form>
</body>
</html>
