<%--
  Created by IntelliJ IDEA.
  User: student07
  Date: 7/13/20
  Time: 8:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Form</title>
    <%@include file="WEB-INF/partials/bootstrap.jsp"%>
</head>
<body>
<%--Navbar included--%>
<%@include file="WEB-INF/partials/navbar.jsp"%>

<%--    HTML FORM --%>
<h1>Please Log In</h1>
<%--    <form action="/login.jsp" method="post">--%>
<%--        <input type="text" name="username" placeholder="Username">--%>
<%--        <br>--%>
<%--        <input type="password" name="password" placeholder="Password">--%>
<%--        <br>--%>
<%--        <input type="submit" value="Log In">--%>
<%--    </form>--%>


<%-- BOOTSTRAP FORM--%>
<%--/login.jsp--%>
<div>
    <form action="/login" method="post">
        <div class="form-group col-6">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp">
        </div>
        <div class="form-group col-6">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>

        <button type="submit" class="btn btn-primary">Check out my profile!</button>
    </form>
</div>

<%--<c:choose>--%>
<%--    <c:when test="${param.username == 'admin' && param.password == 'password'}">--%>
<%--        <%--%>
<%--            response.sendRedirect("/profile.jsp");--%>
<%--        %>--%>
<%--    </c:when>--%>
    <%--        BONUS --%>
    <%--        <c:when test="${param.username == 'stevejobs' && param.password == 'apple'}" >--%>
    <%--            <%--%>
    <%--                response.sendRedirect("https://www.apple.com");--%>
    <%--            %>--%>
    <%--        </c:when>--%>
<%--</c:choose>--%>

<%-- Footer Included--%>
<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
