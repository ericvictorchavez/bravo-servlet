package WishList;


public class DaoFactory {
    private static WishLists WishListsDao;

    public static WishLists getWishListsDao() {
        if (WishListsDao == null) {
            WishListsDao = new ListWishLists();
        }

        return WishListsDao;
    }
}
