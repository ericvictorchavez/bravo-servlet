package WishList;

import java.util.List;


public interface WishLists {
    List<WishList> all();

    void insert(WishList wishList);
}


