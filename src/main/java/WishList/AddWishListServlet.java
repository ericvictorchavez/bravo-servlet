package WishList;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddWishListServlet", urlPatterns = "/WishLists/add-WishLists")
public class AddWishListServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WishLists WishListDao = DaoFactory.getWishListsDao();

        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        double newPrice = Double.parseDouble(request.getParameter("price"));

        WishList newWishList= new WishList(newName, newCategory, newPrice);

        WishListDao.insert(newWishList);
        response.sendRedirect("/WishList");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WishList-app/add-WishLists.jsp").forward(request, response);
    }
}
