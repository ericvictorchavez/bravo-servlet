package WishList;

import java.util.ArrayList;
import java.util.List;


    public class ListWishLists implements WishLists {

        private List<WishList> wishLists = new ArrayList<>();


        public ListWishLists() {
            insert(new WishList("Soap", "Hygiene", 4.00));
            insert(new WishList("Deodorant", "Hygiene",3.00 ));
            insert(new WishList("Hand Sanitizer", "Hygiene", 10.00));

        }



        @Override
        public void insert(WishList wishList) {
            this.wishLists.add(wishList);
        }

        @Override
        public List<WishList> all() {
            return this.wishLists;
        }
    }

