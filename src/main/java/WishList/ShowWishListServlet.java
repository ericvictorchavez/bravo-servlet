package WishList;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowWishListServlet", urlPatterns = "/WishList")
public class ShowWishListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        WishLists WishListsDao = DaoFactory.getWishListsDao();

        List<WishList> WishLists = WishListsDao.all();

        request.setAttribute("ListOfWishes", WishLists);
        request.getRequestDispatcher("/WishList-app/WishLists.jsp").forward(request, response);


    }

}


