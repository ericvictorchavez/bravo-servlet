package sessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "AdminLoginServlet", urlPatterns = "/admin-login")
public class AdminLoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        storing information in our session and use that to restrict access to different parts of our app

//       variable assign to the parameter from our login view
        String username = request.getParameter("username");
        String password = request.getParameter("password");

//        proper  username and password to access application
        boolean validAttempt = username.equals("admin") && password.equals("password123");

        if (validAttempt) {
            request.getSession().setAttribute("user", true);
            // figured out if the login attempt is good, if so, sets an attribute as "user" to the boolean value of true

//            send the "user" to our admin-profile
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/admin-profile");
        } else {
            // if login attempt is no Bueno ...
            response.sendRedirect("/admin-login");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        boolean isUser = false;

        if (session.getAttribute("user") != null) {
            isUser = (boolean) session.getAttribute("user");
            //session.getAttribute - used when we want to retrieve from the session
        }

        if (isUser) {
//            if true , sends user to admin-profile.jsp view
            request.getRequestDispatcher("/cookieSessions/admin-profile.jsp").forward(request,response);
        }
        else{
//            otherwise, keep user on admin-login.jsp view
            request.getRequestDispatcher("/cookieSessions/admin-login.jsp").forward(request,response);
        }
    }
}
