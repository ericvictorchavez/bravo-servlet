import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "PingServlet", urlPatterns ="/Ping")
public class PingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        //        get the printWriter
        PrintWriter writer = response.getWriter();

//        generate the HTML content
        writer.println("<h1>Hello from Ping Servlet!</h1>");

        writer.println("<a href=\"/Pong\">Go to Pong</a>");

        HttpSession session = request.getSession();


        boolean isValidUser = false;

        if (session.getAttribute("user") != null) {
            isValidUser = (boolean) session.getAttribute("user");
        }

        if (isValidUser) {
            request.setAttribute("username", session.getAttribute("username"));
            request.getRequestDispatcher("/ping").forward(request,response);

        }
        else{
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }

    }
}
