package util;

import org.mindrot.jbcrypt.BCrypt;

public class Password {
    public static void main(String[] args) {
//        create a String variable for our password
        String pw = "Password.123";

//        create a String variable for our hashing password
        String hash = BCrypt.hashpw(pw, BCrypt.gensalt());

//        BCrypt.hashpw(pw, BCrypt.gensalt());
        // - generates a hash from the given plaintext password (pw)

//        System.out.println(hash);
//        output for hash will never be the same...

        boolean passwordsMatch = BCrypt.checkpw("mypassword", hash);
//        System.out.println(passwordsMatch);

        passwordsMatch = BCrypt.checkpw("Password.123", hash);
        System.out.println(passwordsMatch);

//        BCrypt.checkpw("Password.123", hash);
//        - Verify that a given plaintext password matches a known hash
    }
}
